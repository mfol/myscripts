#!/bin/bash

# Source and destination directories
SOURCE_DIR="/home/muhammedalessa/Documents/MyNotesForTest/MyNotes"
DEST_DIR="/home/muhammedalessa/Documents/MyNotesForTest/my-notes"
RECIPIENT="muhammedalessa@proton.me"  # Replace with your GPG recipient

# Function to clear the destination directory
clear_dest_dir() {
    echo "Clearing contents of $DEST_DIR except .git directory..."
    # Delete all contents in DEST_DIR except the .git directory
    find "$DEST_DIR" -mindepth 1 -not -path "$DEST_DIR/.git*" -exec rm -rf {} +
}





# Function to encrypt files
encrypt_files() {
    # Find all files in the source directory
    find "$SOURCE_DIR" -type f | while read -r file; do
        # Calculate the relative path
        relative_path="${file#$SOURCE_DIR/}"
        # Calculate the destination path
        dest_path="$DEST_DIR/$relative_path.gpg"
        # Create the destination directory if it does not exist
        mkdir -p "$(dirname "$dest_path")"
        # Encrypt the file
        gpg --output "$dest_path" --encrypt --recipient "$RECIPIENT" "$file"
    done
}

# Function to delete the .obsidian directory in the destination
delete_obsidian_directory() {
    # Find and delete the .obsidian directories and their contents in the destination
    find "$DEST_DIR" -type d -name ".obsidian" -exec rm -rf {} +
}


clear_dest_dir
# Start encryption process
encrypt_files

# Delete .obsidian directories in the destination directory
delete_obsidian_directory

echo "Encryption complete and .obsidian directories deleted from the destination."

