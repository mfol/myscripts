#!/bin/bash

# Interval of check in seconds. Example: 300 seconds = 5 minutes
INTERVAL=1

while true; do
  # Get the battery percentage and status (Charging/Discharging)
  BATTERY_INFO=$(acpi -b)
  BATTERY_PERCENT=$(echo $BATTERY_INFO | grep -Po '\d+%' | tr -d '%')
  BATTERY_STATUS=$(echo $BATTERY_INFO | grep -oP 'Charging|Discharging')

  # If battery level is less than 20% and it is discharging, send a notification
  if [[ "$BATTERY_PERCENT" -lt 15 && "$BATTERY_STATUS" == "Discharging" ]]; then
    echo "Low Battery - $BATTERY_PERCENT%" | dmenu -b
  fi

  # Sleep for the specified interval before checking again
  sleep $INTERVAL
done

