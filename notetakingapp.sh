#!/bin/bash


choose=$(echo -e "Take-Note\nShow-Note" | rofi -dmenu -l 10 -p "Choose:")

if [ $choose = "Take-Note" ];
then
    
    take=$(echo $1 | rofi -dmenu -l -p "Take Note:" -markup-rows -lines 10 -width 50 -font "Lexend 14" -theme-str 'entry {padding: 15px; height: 50px;}')
    
if [ -z "$take" ];then
    exit
else
    filename=$(echo $2 | rofi -dmenu -l -p "Name the Note:" -lines 10 -width 50 -font "Monospace 14" -theme-str 'entry {padding: 15px; height: 50px;}')
    touch ~/Documents/notes/$filename.txt && echo $take > ~/Documents/notes/$filename.txt
#--------------------------------------------------------
sleep 1
if [ -f ~/Documents/notes/$filename.txt ]
then
    notify-send "The Note $filename has been saved"
else
    notify-send "The Note $filename is not saved"
fi
#----------------------------------------------------------
fi
fi
if [ $choose = "Show-Note" ];then
    store=$(ls ~/Documents/notes | rofi -dmenu -l 10 -p "List:")
    
if [ -z "$store" ];then
    exit
else
    rofi -e "$(cat ~/Documents/notes/$store)" \
    -lines 100 \
    -width 100 \
    -font "Lexend 20" \
    -markup-rows \
    -theme-str 'window {background: #282828;} entry {padding: 15px;}'
fi
fi


