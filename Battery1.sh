#!/bin/bash

while true; do
    # Get the battery percentage using acpi
    battery=$(acpi | grep -oP '\d+%' | tr -d '%')

    # Check if battery is less than 20%
    if [ $battery -lt 78 ]; then
        # Notify using rofi
        rofi -e "Low Battery! Current battery level: $battery"
    fi

    # Sleep for some time before checking again (in seconds)
    sleep 300  # 5 minutes
done

