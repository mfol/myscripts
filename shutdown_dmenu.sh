#!/bin/bash

# Define the options to be displayed
options="Shutdown\nReboot\nhibernate\nsuspend"

# Get the choice from the user
choice=$(echo -e $options | dmenu -i -p "What do you want to do?: " -l 10)

# Function to ask for confirmation
confirm() {
    echo -e "Yes\nNo" | dmenu -i -p "$1" -l 10 
}

# Execute the chosen option
case $choice in
    Shutdown)
        answer=$(confirm "Shutdown?")
        if [ "$answer" == "Yes" ]; then
            doas poweroff
        fi
        ;;
    Reboot)
        answer=$(confirm "Reboot?")
        if [ "$answer" == "Yes" ]; then
            doas reboot
        fi
        ;;
    hibernate)
        answer=$(confirm "hibernate?")
        if [ "$answer" == "Yes" ]; then
            doas hibernate
        fi
        ;;
    suspend)
        answer=$(confirm "suspend?")
        if [ "$answer" == "Yes" ]; then
            doas suspend
        fi
        ;;


    *)
        exit 1
        ;;
esac

