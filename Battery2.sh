#!/bin/bash

# Get the battery percentage using acpi
battery=$(acpi | grep -oP '\d+%' | tr -d '%')
status=$(acpi -b | awk '{print $3}' | tr -d ',')

# Check if battery is less than or equal to 20% and status is "Discharging"
if [ $battery -le 20 ] && [ "$status" = "Discharging" ]; then
    # Notify using rofi
    rofi -dmenu -p "Low Battery! Current battery level: $battery"
fi
#########################################################
#!/bin/bash

# Get the battery percentage using acpi
#battery=$(acpi | grep -oP '\d+%' | tr -d '%')
# Check if battery is less than 20%
#if [ $battery -le 20 ]; then
    # Notify using rofi
#    rofi -dmenu -p "Low Battery! Current battery level: $battery"
#fi
